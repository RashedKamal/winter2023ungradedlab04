import java.util.Scanner;
public class Application {
	public static void main(String[] args) {
		/*Student student1 = new Student();
			Student student2 = new Student(); */
			
			/*student1.fulltimeStudent = true;
			System.out.println(student1.fulltimeStudent);
			student1.idNum = 2034147;
			System.out.println(student1.idNum);
			student1.name = "Rashed";
			System.out.println(student1.name);
			student1.applyForScholarship();
			student1.omnivoxLogin();
			System.out.println("");
			
			student2.fulltimeStudent = false;
			System.out.println(student2.fulltimeStudent);
			student2.idNum = 2036199;
			System.out.println(student2.idNum);
			student2.name = "Hannah";
			System.out.println(student2.name);
			student2.applyForScholarship();
			student2.omnivoxLogin();
			System.out.println(""); */
			
			/* Student[] section3 = new Student[3];
			section3[0] = student1;
			section3[1] = student2;
			section3[2] = new Student(); */
			
			//System.out.println(section3[0].fulltimeStudent);
			//System.out.println(section3[1].fulltimeStudent);
			/* 
			System.out.println("Last student value: " + section3[2].amountLearnt);
			section3[2].learn();
			System.out.println("Last student value: " + section3[2].amountLearnt);
			section3[2].learn();
			System.out.println("Last student value: " + section3[2].amountLearnt);
			for (int i = 0; i < section3.length; i++){
				System.out.println(section3[i].amountLearnt);
			} */

			/* Scanner sc = new Scanner(System.in);
			int amountStudied1 = sc.nextInt();
			section3[2].learn(amountStudied1);
			System.out.println("Last student value: " + section3[2].amountLearnt);
			int amountStudied2 = sc.nextInt();
			section3[2].learn(amountStudied2);
			System.out.println("Last student value: " + section3[2].amountLearnt); */

			/* System.out.println(student1.getFullTime());
			System.out.println(student1.getIdNum());
			System.out.println(student1.getName());
			System.out.println(student1.getAmountLearnt());

			student1.setIdNum(2222222);
			student1.setFullTime(true);
			student1.setName("John");
			System.out.println(student1.getIdNum());
			System.out.println(student1.getFullTime());
			System.out.println(student1.getName());
			System.out.println(student1.getAmountLearnt()); */

			// PART 2 CONSTRUCTORS CODE

			Student newStudent = new Student(3424222, "Jim");
			newStudent.setFullTime(false);
			System.out.println(newStudent.getFullTime());
			System.out.println(newStudent.getIdNum());
			System.out.println(newStudent.getName());
			System.out.println(newStudent.getAmountLearnt());
	}	 
}