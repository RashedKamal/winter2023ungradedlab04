public class Student {
	private boolean fulltimeStudent;
	private int idNum;
	private String name;
	public int amountLearnt;

	Student[] section3 = new Student[3];
	public void applyForScholarship() {
		if(this.fulltimeStudent) {
			System.out.println(this.name+ " applied for a Quebec Perspective Scholarship and got accepted!");
		}
		else {
			System.out.println(this.name+ " applied for a Quebec Perspective Scholarship and got rejected.");
		}
	}
	
	public void omnivoxLogin() {
		System.out.println(this.name+ " put their student id "+this.idNum+ " into Omnivox.");
	}

	public void learn(int amountStudied){
		if (amountStudied > 0){
		this.amountLearnt += amountStudied;
		}
	}

	public boolean getFullTime(){
		return this.fulltimeStudent;
	}
	public int getIdNum(){
		return this.idNum;
	}
	public String getName(){
		return this.name;
	}
	public int getAmountLearnt(){
		return this.amountLearnt;
	}

	/* public void setIdNum(int newIdNum){
		this.idNum = newIdNum; // updates
	} */
	public void setFullTime(boolean newFullTime){
		this.fulltimeStudent = newFullTime;
	}
	/*public void setName(String newName){
		this.name = newName;
	}
*/
	//PART 2 CONSTRUCTORS CODE

	public Student(int idNum, String name){
		this.fulltimeStudent = true;
		this.idNum = idNum;
		this.name = name;
		this.amountLearnt = 0;
	}
}